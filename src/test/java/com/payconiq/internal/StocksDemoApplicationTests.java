package com.payconiq.internal;

import com.payconiq.internal.dto.StockDTO;
import com.payconiq.internal.models.Stock;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class StocksDemoApplicationTests extends StockApplicationTestConfig{

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }
    @Test
    public void getStockList() throws Exception {
        String uri = "/api/stocks";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Stock[] stockList = super.mapFromJson(content, Stock[].class);
        assertTrue(stockList.length > 0);
    }
    @Test
    public void createStock() throws Exception {
        String uri = "/api/stocks";
        StockDTO stock = new StockDTO();
        stock.setId(13L);
        stock.setName("Microsoft");
        stock.setCurrentPrice(45000.0);
        stock.setLastUpdate(new Date());
        String inputJson = super.mapToJson(stock);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "stock is added successfully");
    }
    @Test
    public void updateProduct() throws Exception {
        String uri = "/api/stocks/2";
        StockDTO stock = new StockDTO();
        stock.setCurrentPrice(65000.0);
        String inputJson = super.mapToJson(stock);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "stock price is updated Successfully.");
    }
    @Test
    public void deleteProduct() throws Exception {
        String uri = "/api/stocks/2";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "stock is deleted successfully");
    }

}
