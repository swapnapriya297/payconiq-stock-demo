create table STOCK(Id bigint primary key , Name varchar,CurrentPrice double, LastUpdate timestamp);
insert into STOCK(Id,NAME,CURRENTPRICE,LASTUPDATE)
values (7,'Netflix',1100,now()),
       (8,'Zee',2100,now()),
       (9,'Prime',4600,now()),
       (10,'Star',375.90,now()),
       (11,'Disney',6400,now())
;