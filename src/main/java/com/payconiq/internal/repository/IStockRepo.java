package com.payconiq.internal.repository;

import com.payconiq.internal.models.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface IStockRepo extends PagingAndSortingRepository<Stock, Long> {
    List<Stock> findByName(String name);
    Page<Stock> stockList(Pageable pageable);
}
