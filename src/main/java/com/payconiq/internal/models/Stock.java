package com.payconiq.internal.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "stock")
@Data
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="Name")
    private String name;

    @Column(name="CurrentPrice")
    private Double currentPrice;

    @Column(name="LastUpdate")
    private Date lastUpdate;
}
