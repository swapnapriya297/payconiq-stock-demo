package com.payconiq.internal.controller;

import com.payconiq.internal.dto.StockDTO;
import com.payconiq.internal.models.Stock;
import com.payconiq.internal.services.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@Controller
@RequestMapping("/api/stocks")
public class StockController {
    @Autowired
    IStockService stockService;

    @GetMapping
    public ResponseEntity<?> findAllStocks(){
        List<StockDTO> stocks = stockService.findAll();
       return new ResponseEntity<>(stocks,HttpStatus.OK);
    }

    @GetMapping
    public Page<Stock> getStockList(@PageableDefault(size = 10) Pageable pageable) {
       return stockService.getStockList(pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        StockDTO stockDto = stockService.findById(id);
        return new ResponseEntity<>(stockDto,HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody StockDTO stockDTO){

        stockService.create(stockDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStock(@PathVariable Long id){

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updatePrice(@PathVariable Long id,@RequestBody StockDTO stockDTO){
        stockService.update(id,stockDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        stockService.delete(id);
        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
