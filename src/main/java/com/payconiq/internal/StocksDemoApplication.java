package com.payconiq.internal;

import com.payconiq.internal.dto.StockDTO;
import java.util.Date;
import com.payconiq.internal.services.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StocksDemoApplication implements CommandLineRunner {
    @Autowired
    IStockService stockService;
    public static void main(String[] args) {
        SpringApplication.run(StocksDemoApplication.class, args);
    }

    @Override
    public void run(String... args){
      StockDTO stock1 = new StockDTO();
      stock1.setId(1L);
      stock1.setName("Amazon");
      stock1.setCurrentPrice(30000.0);
      stock1.setLastUpdate(new Date());

        StockDTO stock2 = new StockDTO();
        stock2.setId(2L);
        stock2.setName("Google");
        stock2.setCurrentPrice(40000.0);
        stock2.setLastUpdate(new Date());

        StockDTO stock3 = new StockDTO();
        stock3.setId(3L);
        stock3.setName("Payconiq");
        stock3.setCurrentPrice(35000.0);
        stock3.setLastUpdate(new Date());

        StockDTO stock4 = new StockDTO();
        stock4.setId(4L);
        stock4.setName("SIX");
        stock4.setCurrentPrice(20000.0);
        stock4.setLastUpdate(new Date());

        StockDTO stock5 = new StockDTO();
        stock5.setId(5L);
        stock5.setName("WellsFargo");
        stock5.setCurrentPrice(70000.0);
        stock5.setLastUpdate(new Date());

        StockDTO stock6 = new StockDTO();
        stock6.setId(6L);
        stock6.setName("JPMC");
        stock6.setCurrentPrice(20000.0);
        stock6.setLastUpdate(new Date());

    }
}
