package com.payconiq.internal.dto;

import lombok.Data;

import java.util.Date;

@Data
public class StockDTO {

    private Long id;
    private String name;
    private Double currentPrice;
    private Date lastUpdate;
}
