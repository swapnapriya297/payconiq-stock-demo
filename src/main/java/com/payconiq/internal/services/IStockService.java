package com.payconiq.internal.services;

import com.payconiq.internal.dto.StockDTO;

import java.util.List;

import com.payconiq.internal.models.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IStockService {
    public List<StockDTO> findAll();
    public Page<Stock> getStockList(Pageable pageable);
    public StockDTO findById(Long id);
    public void create(StockDTO stockDTO);
    public void update(StockDTO stockDTO);

    public void update(Long id, StockDTO stockDTO);

    public void updateStockById(Long id);
    public void delete(Long id);

}
