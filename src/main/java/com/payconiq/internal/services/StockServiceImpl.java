package com.payconiq.internal.services;

import com.payconiq.internal.dto.StockDTO;
import com.payconiq.internal.models.Stock;
import com.payconiq.internal.repository.IStockRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class StockServiceImpl implements  IStockService {

    @Autowired
    IStockRepo stockRepository;

    @Override
    public List<StockDTO> findAll(){
        List<Stock> stockList = Streamable.of(stockRepository.findAll()).toList();
        List<StockDTO> stockDtoList = new ArrayList<>();
        if(stockList != null){
            stockDtoList = stockList.stream()
                                    .map(stock -> {
                                                    StockDTO dto = new StockDTO();
                                                    BeanUtils.copyProperties(stock , dto);
                                                    return dto;
                                                  })
                                    .collect(Collectors.toList());
        }
        return stockDtoList;
    }

    @Override
    public StockDTO findById(Long id){
        Optional<Stock> stock = stockRepository.findById(id);
        StockDTO stockDto =  null;
        if (stock.isPresent()){
            stockDto = new StockDTO();
            BeanUtils.copyProperties(stock.get(),stockDto);
        }
        return stockDto;
    }

    @Override
    public void create(StockDTO stockDTO) {
        Stock stock = new Stock();
        BeanUtils.copyProperties(stockDTO,stock);
        stock = stockRepository.save(stock);
    }

    @Override
    public void update(StockDTO stockDTO) {

        Long id = stockDTO.getId();
        Optional<Stock> optionalStock = stockRepository.findById(id);
        if(optionalStock.isPresent()){
            Stock stock = optionalStock.get();
            BeanUtils.copyProperties(stockDTO,stock);
            stockRepository.save(stock);
        }
    }
    @Override
    public Page<Stock> getStockList(Pageable pageable) {
        return stockRepository.findAll(pageable);
    }
    @Override
    public void update(Long id,StockDTO stockDTO) {
        Optional<Stock> stockOptional = stockRepository.findById(id);
        if(stockOptional.isPresent()){
            Stock stock = stockOptional.get();
            //stock.setCurrentPrice(stockPrice);
            BeanUtils.copyProperties(stockDTO,stock);
            stockRepository.save(stock);
        }
    }

    @Override
    public void updateStockById(Long id) {
        Optional<Stock> optionalStock = stockRepository.findById(id);
        if(optionalStock.isPresent())
        {
            Stock stock = optionalStock.get();

        }

    }

    @Override
    public void delete(Long id) {
       stockRepository.deleteById(id);
    }


}
